import Foundation
import Cocoa

class View: NSView {

  var tree = RTree(m: 2, M: 4)
  
  override var acceptsFirstResponder: Bool {
   return true
  }
  
  override func awakeFromNib() {
    
//    tree.add(rectangle: CGRect(x: 20, y: 20, width: 100, height: 100), name: "1")
//    tree.add(rectangle: CGRect(x: 100, y: 200, width: 100, height: 100), name: "2")
//    tree.add(rectangle: CGRect(x: 50, y: 200, width: 100, height: 100), name: "3")
//
//    tree.add(rectangle: CGRect(x: 380, y: 200, width: 50, height: 50), name: "4")
//    tree.add(rectangle: CGRect(x: 380, y: 100, width: 50, height: 50), name: "5")
//    tree.add(rectangle: CGRect(x: 380, y: 300, width: 50, height: 50), name: "6")
//    tree.add(rectangle: CGRect(x: 380, y: 400, width: 50, height: 50), name: "7")

    setNeedsDisplay(self.bounds)
    
    self.window?.acceptsMouseMovedEvents = true
  }
  
  override func draw(_ dirtyRect: NSRect) {
    tree.root?.draw()
   
    if let draggingRect = draggingRect {
      let ctx = NSGraphicsContext.current!.cgContext

      let color = NSColor.darkGray
      ctx.setLineDash(phase: 0, lengths: [])
      
      color.setStroke()
      color.withAlphaComponent(0.1).setFill()

      ctx.fill(draggingRect)
      ctx.stroke(draggingRect)
    }
  }
  
  var dragging = false
  var dragStart: CGPoint? = nil
  var dragEnd: CGPoint? = nil

  var draggingRect: CGRect? {
    guard dragging == true else { return nil }
    guard let dragStart = dragStart else { return nil }
    guard let dragEnd = dragEnd else { return nil }

    var rect: CGRect = .zero
    if dragEnd.x >  dragStart.x {
      rect.origin.x = dragStart.x
      rect.size.width = dragEnd.x - dragStart.x
    }else {
      rect.origin.x = dragEnd.x
      rect.size.width = -dragEnd.x + dragStart.x
      
    }
    
    if dragEnd.y >  dragStart.y{
      rect.origin.y = dragStart.y
      rect.size.height = dragEnd.y - dragStart.y
      
    }else {
      rect.origin.y = dragEnd.y
      rect.size.height = -dragEnd.y + dragStart.y
    }
    
    if rect.size.width < 5 || rect.size.height < 5 { return nil }
    return rect
  }
  override func mouseMoved(with event: NSEvent) {
    let location = self.convert(event.locationInWindow, from: self.window?.contentView)
    let nodes = tree.search(CGRect(origin: location, size: CGSize(width: 1, height: 1)))
    
    tree.root?.resetHighlight()
    
    nodes.forEach {
      $0.0.isHighlighted = true
      $0.1.forEach { $0.isHighlighted = true }
    }
    
    setNeedsDisplay(self.bounds)
  }
  
  override func mouseDown(with event: NSEvent) {
    let location = self.convert(event.locationInWindow, from: self.window?.contentView)
    let nodes = tree.search(CGRect(origin: location, size: CGSize(width: 1, height: 1)))
      .filter { $0.0.isHighlighted }
    
    if nodes.isEmpty {
      dragging = true
      dragStart = location
      dragEnd = nil
    }else {
      nodes.forEach { tree.remove($0) }

      var screenLocation = event.locationInWindow
      screenLocation.x += self.window!.frame.origin.x
      screenLocation.y += self.window!.frame.origin.y

      ObjCBridge.poof(screenLocation, size: NSMakeSize(100, 100))
    }
    setNeedsDisplay(self.bounds)
  }
  
  override func mouseDragged(with event: NSEvent) {
    let location = self.convert(event.locationInWindow, from: self.window?.contentView)
    dragEnd = location
    setNeedsDisplay(self.bounds)
  }
  
  override func mouseUp(with event: NSEvent) {
    
    if let draggingRect = draggingRect {
      tree.add(rectangle: draggingRect, name: String(REntityNode.entityNumber))
    }
    
    setNeedsDisplay(self.bounds)

    dragging = false
    dragEnd = nil
    dragStart = nil
  }
}



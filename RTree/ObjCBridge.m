#import "ObjCBridge.h"
#import <Cocoa/Cocoa.h>

@implementation ObjCBridge

+(void)poof:(NSPoint) location size:(NSSize) size {
  
  NSShowAnimationEffect(NSAnimationEffectDisappearingItemDefault,
                        location,
                        size,
                        nil,
                        nil,
                        nil);
  
}


@end

import Foundation

extension CGRect {
  func unionArea(_ r2: CGRect) -> CGFloat {
    let unionRect = self.union(r2)
    return unionRect.width * unionRect.height
  }
  
  var area: CGFloat {
    return width * height
  }
}

class REntityNode: RNode {
  static var entityNumber: Int = 0

  var name = ""
  override var id: String {
    return name
  }

  init(M: Int, rectangle: CGRect? = nil, name: String) {
    super.init(M: M, rectangle: rectangle, level: 0)
    
    self.name = name
    REntityNode.entityNumber += 1
  }
}

class RNode: Equatable {
  static func == (lhs: RNode, rhs: RNode) -> Bool {
    return lhs.id == rhs.id
  }
  
  static var counter: Int = 0
  
  var region: CGRect! = nil
  var number: Int!
  var id: String { return "R\(number!)" }
  var children: [RNode] = []
  var level: Int
  var M: Int
  
  // FOR DRAWING EXTENSION
  var isHighlighted = false
  
  init(M: Int, rectangle: CGRect? = nil, level: Int = 0) {
    region = rectangle

    number = RNode.counter
    
    self.M = M
    self.level = level
    
    if type(of: self) == RNode.self {
      RNode.counter += 1
    }
  }
  
  func clear() {
    region = nil
    children = []
  }
  
  func size() -> Int {
    if level == 0 { return children.count }
    else {
      return children.reduce(0) { $0 + $1.size() }
    }
  }
  
  func search(_ target: CGRect, path: NodePath = [] ) -> [(REntityNode, NodePath)] {
    if level == 0 {
      return children.compactMap {
        if $0.region.intersects(target) {
          return ($0 as! REntityNode, path + [self])
        }else {
          return nil
        }
      }
      
    }else if region.intersects(target) {
      return children.reduce([]) { $0 + $1.search(target, path: path + [self]) }
    }
    
    return []
  }
  
  func adjustRegion() {
    
    var adjusted: CGRect? = nil
    for child in children {
      if adjusted == nil {
        adjusted = child.region
      }
      adjusted = adjusted!.union(child.region)
    }
    region = adjusted ?? .zero
  }
  
  func addRNode(node: RNode) {
    guard let nodeRegion = node.region else { return }
    children.append(node)
    region = region?.union(nodeRegion) ?? nodeRegion
  }
  
  func condenseTree(path: NodePath, m: Int) -> [RNode] {
    var node = self
    var Q: NodePath = []
    var last: RNode? = nil
    
    for parent in path.reversed() {
      if node.children.count < m {
        parent.removeRNode(node: node)
        Q.insert(node, at: 0)
      }else {
        node.adjustRegion()
        if last == nil {
          last = node
        }
      }
      
      node = parent
    }
    
    return Q
  }
  
  func leafOrder() -> [REntityNode] {
    if level == 0 {
      return children as! [REntityNode]
    }else {
      return children.reduce([]) { $0 + $1.leafOrder() }
    }
  }
  
  
  func removeRNode(node: RNode) {
    if let idx =  children.index(of: node) {
      children.remove(at: idx)
    }
  }
  
  func removeEntry(_ idx: Int) {
    guard idx < children.count else { return }
    children.remove(at: idx)
  }

  
  func leafPath(to rectangle: CGRect, pathSoFar path: NodePath = []) -> (RNode, NodePath) {
    if level == 0 { return (self, path) }

    var choice: RNode! = nil
    var lastArea: CGFloat = -1
    for child in children {
      let area = child.region.unionArea(rectangle)
      if choice == nil || area < lastArea {
        lastArea = area
        choice = child
      }else if area == lastArea && choice.region.area > child.region.area {
        choice = child
      }
    }
 
    return choice.leafPath(to: rectangle, pathSoFar: path + [self])
  }

  //MARK:-  Quadratic-Cost Algorithm
  func split(with node: RNode, m: Int) -> RNode {
    let newSibling = RNode(M: M, level: level)
    
    var entries = children
    entries.append(node)
    clear()
    RNode.pickSeeds(for: (self, newSibling), from: &entries)
    
    while entries.count > 0 {
      if children.count + entries.count <= m {
        entries.forEach { self.addRNode(node: $0) }
        break
      }
      
      if newSibling.children.count + entries.count <= m {
        entries.forEach { newSibling.addRNode(node: $0) }
        break
      }
      
      RNode.pickNext(for: (self, newSibling), remaining: &entries)
    }
    
    return newSibling
  }
  
  static func pickSeeds(for parents: (RNode, RNode), from entries: inout [RNode]) {
    
    var maxArea: CGFloat = 0
    var chosen: (RNode, RNode)? = nil
    for i0 in 0..<entries.count-1 {
      let node0 = entries[i0]
      for i1 in i0+1..<entries.count {
        let node1 = entries[i1]
        
        let d = node0.region.unionArea(node1.region) - node0.region.area - node1.region.area
        if chosen == nil || d > maxArea {
          maxArea = d
          chosen = (node0, node1)
        }
      }
    }
    
    if let chosen = chosen {
      parents.0.addRNode(node: chosen.0)
      parents.1.addRNode(node: chosen.1)
      
      if let idx = entries.index(of: chosen.0) { entries.remove(at: idx) }
      if let idx = entries.index(of: chosen.1) { entries.remove(at: idx) }
    }
  }
  
  static func pickNext(for parents: (RNode, RNode), remaining: inout [RNode]) {
    guard remaining.count > 0 else { return }
    var maxDiff: CGFloat = 0
    var chosen: RNode? = nil
    
    for entry in remaining {
      let d0 = parents.0.region.unionArea(entry.region) - entry.region.area - parents.0.region.area
      let d1 = parents.1.region.unionArea(entry.region) - entry.region.area - parents.1.region.area
      
      if chosen == nil || abs(d0 - d1) > maxDiff {
        maxDiff = abs(d0 - d1)
        chosen = entry
     }
    }
    
    let d0 = parents.0.region.unionArea(chosen!.region)
    let d1 = parents.1.region.unionArea(chosen!.region)

    let parentToAdd = d0 < d1 ? parents.0 : parents.1
    parentToAdd.addRNode(node: chosen!)
    
    if let idx = remaining.index(of: chosen!) { remaining.remove(at: idx) }
  }
  
  
  func description() -> String {
    var rep = ""
    for child in children {
      rep += child.description()
    }
    
    return "(\(id):\(region!): \(rep))"
    
  }
}


//MARK:- DRAWING
import CoreGraphics
import UIKit

extension RNode {

  static let colors: [UIColor] = [#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1), #colorLiteral(red: 0.5738074183, green: 0.5655357838, blue: 0, alpha: 1), #colorLiteral(red: 0.3084011078, green: 0.5618229508, blue: 0, alpha: 1), #colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 1), #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1), #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), #colorLiteral(red: 0.5818830132, green: 0.2156915367, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1), #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1), #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)]
  
  func draw(_ defaultColor: UIColor? = nil) {
    guard let ctx = UIGraphicsGetCurrentContext() else { return }

    let idx = Double(number).truncatingRemainder(dividingBy:  Double(RNode.colors.count))
    var color = RNode.colors[Int(abs(idx))]
    
    if type(of: self) == REntityNode.self {
      color = defaultColor!
    }
    
    if isHighlighted {
      ctx.setLineWidth(3)
    }else {
      ctx.setLineWidth(1)
    }
    
    if self is REntityNode {
      if isHighlighted {
        color.withAlphaComponent(0.2).setFill()
      }else {
        color.withAlphaComponent(0.1).setFill()
      }
      
      ctx.setLineDash(phase: 0, lengths: [])
      
      color.setStroke()
      
      ctx.fill(region.insetBy(dx: 2-CGFloat(2*level), dy: 2-CGFloat(2*level)))
      ctx.stroke(region.insetBy(dx: 2-CGFloat(2*level), dy: 2-CGFloat(2*level)))
      
    }else {
      
      ctx.setLineDash(phase: 0, lengths: [2,2])
      
      color.setStroke()
      ctx.stroke(region.insetBy(dx: -CGFloat(2*level), dy: -CGFloat(2*level)))
    }
    
    
    if self is REntityNode {
      let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 10), .foregroundColor: color]

      let size = (id as NSString).size(withAttributes: attrs)
      let point = CGPoint(x: region.midX - size.width/2, y: region.midY - size.height/2)
      (id as NSString).draw(at: point, withAttributes: attrs)

    }else {
      let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 20), .foregroundColor: color.withAlphaComponent(0.2)]

      let size = (id as NSString).size(withAttributes: attrs)
      let point = CGPoint(x: region.midX - size.width/2, y: region.midY - size.height/2)

      (id as NSString).draw(at: point, withAttributes: attrs)
    }
    
    children.forEach { $0.draw(color) }
  }
  
  func resetHighlight() {
    isHighlighted = false
    children.forEach { $0.resetHighlight() }
  }
  
}

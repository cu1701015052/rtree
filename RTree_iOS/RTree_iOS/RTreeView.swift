//
//  RTreeView.swift
//  RTree_iOS
//
//  Created by Masatoshi Nishikata on 20/01/19.
//  Copyright © 2019 Masatoshi Nishikata. All rights reserved.
//

import UIKit
import CoreGraphics

class RTreeView: UIView {

  var tree = RTree(m: 2, M: 4)

  override func draw(_ rect: CGRect) {
    guard let ctx = UIGraphicsGetCurrentContext() else { return }
    
    tree.root?.draw()
    
    if let draggingRect = draggingRect {
      
      let color = UIColor.darkGray
      ctx.setLineDash(phase: 0, lengths: [])
      
      color.setStroke()
      color.withAlphaComponent(0.1).setFill()
      
      ctx.fill(draggingRect)
      ctx.stroke(draggingRect)
    }
    
    if let dragStart = dragStart {
      ctx.setLineDash(phase: 0, lengths: [2,2])
      let color = UIColor.darkGray
      color.setStroke()
      let ellipse = CGRect(x: dragStart.x - 5, y: dragStart.y - 5, width: 10, height: 10)
      ctx.strokeEllipse(in: ellipse)
    }
  }

  var dragging = false
  var dragStart: CGPoint? = nil
  var dragEnd: CGPoint? = nil
  
  var draggingRect: CGRect? {
    guard dragging == true else { return nil }
    guard let dragStart = dragStart else { return nil }
    guard let dragEnd = dragEnd else { return nil }
    
    var rect: CGRect = .zero
    if dragEnd.x >  dragStart.x {
      rect.origin.x = dragStart.x
      rect.size.width = dragEnd.x - dragStart.x
    }else {
      rect.origin.x = dragEnd.x
      rect.size.width = -dragEnd.x + dragStart.x
      
    }
    
    if dragEnd.y >  dragStart.y{
      rect.origin.y = dragStart.y
      rect.size.height = dragEnd.y - dragStart.y
      
    }else {
      rect.origin.y = dragEnd.y
      rect.size.height = -dragEnd.y + dragStart.y
    }
    
    if rect.size.width < 5 || rect.size.height < 5 { return nil }
    return rect
  }
  
//  override func mouseMoved(with event: NSEvent) {
//    let location = self.convert(event.locationInWindow, from: self.window?.contentView)
//    let nodes = tree.search(CGRect(origin: location, size: CGSize(width: 1, height: 1)))
//
//    tree.root?.resetHighlight()
//
//    nodes.forEach {
//      $0.0.isHighlighted = true
//      $0.1.forEach { $0.isHighlighted = true }
//    }
//
//    setNeedsDisplay(self.bounds)
  //  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else { return }
    let location = touch.location(in: self)
    let nodes = tree.search(CGRect(origin: location, size: CGSize(width: 1, height: 1)))

    let highlightedNodes = nodes.filter { $0.0.isHighlighted }

    
    if nodes.isEmpty {
      tree.root?.resetHighlight()

      if dragging {
        
        dragEnd = location
        if let draggingRect = draggingRect {
          tree.add(rectangle: draggingRect, name: String(REntityNode.entityNumber))
        }
        
        setNeedsDisplay(self.bounds)
        
        dragging = false
        dragEnd = nil
        dragStart = nil
        
      }else {
        dragging = true
        dragStart = location
        dragEnd = nil
      }
    }else {
      if highlightedNodes.count == 0 {
        tree.root?.resetHighlight()

        nodes.forEach {
          $0.0.isHighlighted = true
          $0.1.forEach { $0.isHighlighted = true }
        }
      }else {
        nodes.forEach { tree.remove($0) }
      }
    }
    setNeedsDisplay(self.bounds)
  }
  
}

import CoreGraphics

typealias NodePath = [RNode]

class RTree {
  var root: RNode? = nil
  var m: Int!
  var M: Int!
  
  init(m: Int = 2, M: Int = 4) {
    root = nil
    self.m = m
    self.M = M
  }
  
  func search(_ rectangle: CGRect) -> [(REntityNode, NodePath)] {
    return root?.search(rectangle) ?? []
  }
  
  func add(rectangle: CGRect, name: String) {
    
    let nodeToAdd = REntityNode(M: M, rectangle: rectangle, name: name)

    if root == nil {
      root = RNode(M: M, rectangle: rectangle)
      root!.addRNode(node: nodeToAdd)
      
    }else {
      let (lastNode, path) = root!.leafPath(to: rectangle)
      
      var branchedOutNode: RNode? = nil
      if lastNode.children.count < M {
        lastNode.addRNode(node: nodeToAdd)
      }else {
        branchedOutNode = lastNode.split(with: nodeToAdd, m: m)
      }
      
      if let newNode = adjustTree(node: lastNode, sibling: branchedOutNode, path: path) {
        let newRoot = RNode(M: M, level: newNode.level + 1)
        newRoot.addRNode(node: newNode)
        newRoot.addRNode(node: root!)
        root = newRoot
      }
    }
  }
  
  func adjustTree(node: RNode, sibling: RNode?, path: NodePath) -> RNode? {
    if path.count == 0 { return sibling }
    var sibling = sibling

    for parent in path.reversed() {
      
      parent.region = parent.region.union(node.region)
      
      if sibling != nil {
        if parent.children.count < M {
          parent.addRNode(node: sibling!)
          sibling = nil
        }else {
          sibling = parent.split(with: sibling!, m: m)
        }
      }
      
      if parent == root { break }
    }
    
    return sibling
  }
  
  func remove(_ nodeAndPath: (REntityNode, NodePath)) {
    let node = nodeAndPath.0
    var path = nodeAndPath.1
    
    let parent = path.last!
    path = Array(path.dropLast())
    
    parent.removeRNode(node: node)
    
    if parent == root {
      root?.adjustRegion()
    }else {
      let Q = parent.condenseTree(path: path, m: m)
      
      Q.forEach {
        $0.leafOrder().forEach {
         self.add(rectangle: $0.region, name: $0.id)
        }
      }
      
      while root!.children.count == 1 && root!.level > 0 {
        root = self.root?.children.first
      }
    }
    
    if root?.children.count == 0 {
      root = nil
    }
  }
  
  func description() -> String {
    return root?.description() ?? ""
  }
}

